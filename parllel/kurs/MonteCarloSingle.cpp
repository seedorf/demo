#include <iostream>
#include <ctime>

const int A = 0;
const int B = 1000;
const int h = 2000;
const double step = 0.001;

double func(double x) {
    return(x / 2);
}

void singleProc();

int main() {
    singleProc();
    return 0;
}

void singleProc() {
    unsigned int startTime = clock();
    std::srand(unsigned(std::time(0)));
    int countIn = 0;
    int countAll = 0; 

    double currentStep = A;
    while(currentStep <= B) {
        int randNum = std::rand() % h;
        if (randNum <= func(currentStep)) {
            countIn++;
        }
        countAll++;
        currentStep += step;
    }
 
    double square = (double) countIn / countAll *  (B - A) * h;
    unsigned int endTime = clock();
    std:: cout << "square = " << square << std::endl;
    std:: cout << "time" << endTime << std::endl; 
}
