#include <iostream>
#include <ctime>
#include <mpi.h>

const int A = 0;
const int B = 1000;
const int h = 2000;
const double step = 0.01;

double func(double x) {
    return(x / 2);
}

int main(int arc, char **argv) {

    int size, rank, msgtag = 5;
	MPI_Status status;

	if (MPI_Init(&arc, &argv) != MPI_SUCCESS) return 1;
	if (MPI_Comm_size(MPI_COMM_WORLD, &size) != MPI_SUCCESS) {
		MPI_Finalize();
		return 2;
	}
	if (MPI_Comm_rank(MPI_COMM_WORLD, &rank) != MPI_SUCCESS) {
		MPI_Finalize();
		return 3;
	}

    // распределение значений
    int n = (B - A) / step; // 100k
    int n1 = n / size;
    int n2 = n % size;
    std::cout << "n="<< n << std::endl;
    int iStart;
    if (rank < n2) {
        n1++;
        iStart = rank * n1;        
    }
    else {
        iStart = rank * n1 + n2;
    }


    int *sizes = new int[size]; // 20k
	MPI_Allgather(&n1, 1, MPI_INT, sizes, 1, MPI_INT, MPI_COMM_WORLD);

	int *disps = new int[size];
	MPI_Allgather(&iStart, 1, MPI_INT, disps, 1, MPI_INT, MPI_COMM_WORLD);

    int count = 0;
    int globalCount = 0;
    std::srand(unsigned(std::time(0) + rank));
    double curX = iStart;
      //std::cout << "iStart" <<iStart << " ";
        // std::cout << "n1" <<n1 << std::endl;
    for(int i = iStart; i < iStart + n1; i++) {  
       
        int randNum = std::rand() % (h + 1);
        if (randNum < func(curX * step)) {
            count++;
            // std::cout << "count" <<count << " ";
            // std::cout << "randNum" <<randNum << " ";
            // std::cout << "func" <<func(curX) << " ";
        }

        // std::cout << "randNum" <<randNum << " ";
          //   std::cout << "func" <<func(curX) << " ";
        std::cout << "curX" << curX  / 100<<std::endl;

        curX += 1;//step;
        
    }
    //std::cout << count<< std::endl;
    // std::cout << " " << globalCount<< std::endl;

    MPI_Allreduce(&count, &globalCount, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD );
    double finalSquare = (double) globalCount / n * ((B - A) * h);
     //std::cout << globalCount << " " << finalSquare << std::endl;

   // std::cout << globalCount << std::endl;
    // std::cout << finalSquare << std::endl;

    //std::cout << count << " ";
    //double square    = (double) countIn / countAll * ( (B - A) * h );
    // double square = (double) count / n1 * ()
    MPI_Finalize();
    return 0;
}
