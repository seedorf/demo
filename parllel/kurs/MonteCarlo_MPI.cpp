#include <iostream>
#include <ctime>
#include <mpi.h>

const int A = 0;
const int B = 1000;
const int h = 2000;
const double step = 0.0001; 

double func(double x) {
    return(x / 2);
}

int main(int arc, char **argv) {

    int size, rank, msgtag = 5;
	MPI_Status status;

	if (MPI_Init(&arc, &argv) != MPI_SUCCESS) return 1;
	if (MPI_Comm_size(MPI_COMM_WORLD, &size) != MPI_SUCCESS) {
		MPI_Finalize();
		return 2;
	}
	if (MPI_Comm_rank(MPI_COMM_WORLD, &rank) != MPI_SUCCESS) {
		MPI_Finalize();
		return 3;
	}

    int n = (B - A);
    int n1 = n / size;
    int n2 = n % size;

    int iStart;
    if (rank < n2) {
        n1++;
        iStart = rank * n1;        
    }
    else {
        iStart = rank * n1 + n2;
    }

    int count = 0;
    int allCount = 0;
    int globalCount = 0;
    int allGlobalCount = 0;

    std::srand(unsigned(std::time(0) + rank));

    int finalPoint = iStart + n1;
    double curX = iStart;

    do {
        int randNum = std::rand() % (h);
        if (randNum <= func(curX)) {
            count++;
        }
        allCount++;
        curX += step;
    }while(curX < finalPoint);
    
    MPI_Allreduce(&allCount, &allGlobalCount, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(&count, &globalCount, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD );
    double finalSquare = (double) globalCount / allGlobalCount * ((B - A) * h);

    if (!rank) {
        printf("%f", finalSquare);
    }

    MPI_Finalize();
    return 0;
}
