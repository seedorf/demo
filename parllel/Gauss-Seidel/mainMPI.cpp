#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include <fstream>

#define n 500
#define eps 0.001

using namespace std;

int main(int arc, char **argv) {

	int size, rank, msgtag = 12;
	MPI_Status status;

	if (MPI_Init(&arc, &argv) != MPI_SUCCESS) return 1;
	if (MPI_Comm_size(MPI_COMM_WORLD, &size) != MPI_SUCCESS) {
		MPI_Finalize();
		return 2;
	}
	if (MPI_Comm_rank(MPI_COMM_WORLD, &rank) != MPI_SUCCESS) {
		MPI_Finalize();
		return 3;
	}

	double sum = 0;
	double **a = new double*[n];
	double *b = new double[n];
	double *x = new double[n];

	for (int i = 0; i < n; i++)
	{
		a[i] = new double[n];
		x[i] = 0.0;
	}

	ifstream finA("matrAf.txt");
	ifstream finB("matrBf.txt");

	if ((!finA.is_open()) || (!finB.is_open()))
	{
		printf("Error in input files!\n");
		return 1;
	}

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			finA >> a[i][j];

		finB >> b[i];
	}

	finA.close();
	finB.close();

	double tn = MPI_Wtime();

	int n1 = n / size;
	int n2 = n % size;
	int iStart;
	if (rank < n2)
	{
		n1++;
		iStart = rank*n1;
	}
	else
	{
		iStart = rank*n1 + n2;
	}

	int *sizes = new int[size];
	MPI_Allgather(&n1, 1, MPI_INT, sizes, 1, MPI_INT, MPI_COMM_WORLD);
	
	int *disps = new int[size];
	MPI_Allgather(&iStart, 1, MPI_INT, disps, 1, MPI_INT, MPI_COMM_WORLD);
	
	double glmax = x[0], globalMax;
	do
	{
		for (int i = iStart; i < iStart + n1; i++)
		{
			double s = 0.0;

			double xlast = x[i];

			for (int j = 0; j < n; j++)
				if (i != j) s += a[i][j] * x[j];

			x[i] = (b[i] - s) / a[i][i];

			if (glmax < abs(x[i] - xlast)) glmax = abs(x[i] - xlast);
		}
		
		MPI_Allreduce(&glmax, &globalMax, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
		
		MPI_Allgatherv(MPI_IN_PLACE , n1, MPI_DOUBLE, x, sizes, disps, MPI_DOUBLE, MPI_COMM_WORLD); // x + iStart
	} while (globalMax > eps);
	
	double tk = MPI_Wtime();

	if (!rank) 
	{ 
		FILE *f;
		f = fopen("Results.txt", "w");

		for (int i = 0; i < n; i++)
		{
			fprintf(f, "x[%d] = %f\n", i, x[i]);
		}

		fprintf(f, "\nTime = %f\n", tk - tn);
		fclose(f);
	}
	
	MPI_Finalize();
	return 0;
}