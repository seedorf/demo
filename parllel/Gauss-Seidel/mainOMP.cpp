#define _CRT_SECURE_NO_WARNINGS

#include <omp.h>
#include <fstream>
#include <iostream>
#include <ctime>

#define n 500
#define eps 0.001

using namespace std;

int main()
{
	double *x = new double[n];
	double *xn = new double[n];
	double **a = new double*[n];
	double *b = new double[n];
	double max, s1 = 0.0, s2 = 0.0;

	for (int i = 0; i < n; i++)
	{
		a[i] = new double[n];
		x[i] = 0.0;
	}
	double tn = clock();
	
	std::ifstream finA("matrAf.txt");
	std::ifstream finB("matrBf.txt");

	if ((!finA.is_open()) || (!finB.is_open()))
	{
		std::cout << ("Error in input files!\n");
		system("pause");
		return 1;
	}

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			finA >> a[i][j];// a[i][j];

		finB >> b[i];
	}

	finA.close();
	finB.close();

	do {
#pragma omp parallel
		{
			for (int i = 0; i < n; i++)
			{
#pragma omp master
				{
					s1 = s2 = 0.0;
				}
#pragma omp barrier

#pragma omp for reduction(+: s1)
				for (int j = 0; j < i; j++)
					s1 += a[i][j] * xn[j];

#pragma omp for reduction(+: s2)
				for (int j = i + 1; j < n; j++)
					s2 += a[i][j] * x[j];

#pragma omp master
				{
					xn[i] = 1.0 / a[i][i] * (b[i] - s1 - s2);
				}
#pragma omp barrier
			}
		}

		max = abs(x[0] - xn[0]);

		for (int i = 1; i < n; i++)
		{
			if (max < abs(x[i] - xn[i])) max = abs(x[i] - xn[i]);
		}

		for (int i = 0; i < n; i++)
		{
			x[i] = xn[i];
		}
	} while (max > eps);

	FILE *f;
	f = fopen("Res.txt", "w");

	for (int i = 0; i < n; i++)
	{
		fprintf(f, "x[%d] = %f\n", i, x[i]);
	}

	cout << (clock() - tn) / 1000.0 << endl;

	fclose(f);

	system("pause");
	return 0;
}