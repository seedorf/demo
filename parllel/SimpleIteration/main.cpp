

#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include <fstream>
#include <iostream>

#define n 500
#define eps 0.001

int main(int arc, char **argv) {

	int size, rank, msgtag = 12;
	MPI_Status status;

	if (MPI_Init(&arc, &argv) != MPI_SUCCESS) return 1;
	if (MPI_Comm_size(MPI_COMM_WORLD, &size) != MPI_SUCCESS) {
		MPI_Finalize();
		return 2;
	}
	if (MPI_Comm_rank(MPI_COMM_WORLD, &rank) != MPI_SUCCESS) {
		MPI_Finalize();
		return 3;
	}

	double sum = 0;
	double **a = new double*[n];
	double *b = new double[n];
	double *x = new double[n];
	double *xn = new double[n];
	for (int i = 0; i < n; i++)
	{
		a[i] = new double[n];
		x[i] = xn[i] = 0.0;
	}
	double tn = MPI_Wtime();
	std::ifstream finA("matrAf.txt");
	std::ifstream finB("matrBf.txt");

	if ((!finA.is_open()) || (!finB.is_open()))
	{
		printf("Error in input files!\n");
		return 1;
	}

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			finA >> a[i][j];

		finB >> b[i];
	}

	finA.close();
	finB.close();

    double ** curArray = new double* [n];
    for (int i = 0; i < n; i++) {
        double * line = new double[n];
        for (int j = 0; j < n; j++) {
            int currentNum = random % n;
            line[j] = (double)currentNum;
        }
        curArray[i] = line;
    }


	//распределение
	int n1 = n / size;
	int n2 = n % size;
	int iStart;
	if (rank < n2)
	{
		n1++;
		iStart = rank * n1;
	}
	else
	{
		iStart = rank * n1 + n2;
	}

	//собираем массив размеров на каждом
	int *sizes = new int[size];
	MPI_Allgather(&n1, 1, MPI_INT, sizes, 1, MPI_INT, MPI_COMM_WORLD);

	//собираем массив смещений на каждом
	int *disps = new int[size];
	MPI_Allgather(&iStart, 1, MPI_INT, disps, 1, MPI_INT, MPI_COMM_WORLD);

	double glmax, sum1, sum2, globalMax;
	do
	{
		for (int i = iStart; i < iStart + n1; i++)
		{
			sum1 = sum2 = 0.0;
			for (int j = 0; j < i; j++)
				sum1 += a[i][j] * x[j];
			for (int j = i + 1; j < n; j++)
				sum2 += a[i][j] * x[j];
			xn[i] = 1.0 / a[i][i] * (b[i] - sum1 - sum2);
		}
		glmax = fabs(xn[iStart] - x[iStart]);
		x[iStart] = xn[iStart];
		for (int i = iStart + 1; i < iStart + n1; i++)
		{
			if (fabs(xn[i] - x[i]) > glmax)
				glmax = fabs(xn[i] - x[i]);
			//x[i] = xn[i];
		}

		MPI_Allreduce(&glmax, &globalMax, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

		MPI_Allgatherv(xn + iStart, n1, MPI_DOUBLE, x, sizes, disps, MPI_DOUBLE, MPI_COMM_WORLD);
	} while (globalMax > eps);

	if (!rank)
	{
		FILE *f;
		f = fopen("Res.txt", "w");

		for (int i = 0; i < n; i++)
		{
			fprintf(f, "x[%d] = %f\n", i, x[i]);
		}
		fclose(f);
		std::cout << MPI_Wtime() - tn << std::endl;
	}
	MPI_Finalize();
	return 0;
}
