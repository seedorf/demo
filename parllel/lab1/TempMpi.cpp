#include <iostream>
#include <mpi.h>
#include <math.h>
#include <stdio.h>

double f0(double t)
{	
	return 3 * (0.3 - 2 * t);
}

double f1(double t)
{
	return 1.38;
}

double g(double x)
{
	return 0.9 + 2 * x * (1 - x);
}

int main(int arc, char **argv) {
	//Определение начальных значений переменных
	int size, rank, msgtag = 12;
	double a = 1.0, len = 300.0, x = 0.0, time = 0.0, tmax = 100.0, tau = 0.001, h = 0.1;
	double r = a * tau / (h*h);
	int N = (int)(len / h) + 1;
	int n, l = 0, i, j;

	if (MPI_Init(&arc, &argv) != MPI_SUCCESS) return 1;
	if (MPI_Comm_size(MPI_COMM_WORLD, &size) != MPI_SUCCESS) {
		MPI_Finalize();
		return 2;
	}
	if (MPI_Comm_rank(MPI_COMM_WORLD, &rank) != MPI_SUCCESS) {
		MPI_Finalize();
		return 3;
	}

	MPI_Status status;
	//Распределение отрезков по процессам
	int l1 = N / size;
	int l2 = N % size;

	int *kol = new int[size];
	for (i = 0; i < size; i++) kol[i] = l1;

	if (l2) {
		if (rank == size - 1) l1++;
		l2--;
		kol[size - 1]++;
	}
	if (l2) {
		if (rank < l2) l1++;
		for (i = 0; i < l2; i++) kol[i]++;
	}

	n = l1;
	if ((!rank) || (rank == size - 1)) n++; else n += 2;
	double *u = new double[n];
	double *L = new double[n];
	double *M = new double[n];


	if (rank) {
		int sum = 0;
		for (i = 0; i < rank; i++) sum += kol[i];
		l = sum;
		sum--;
		x = h * sum; //Вычисляем смещение для каждого промежутка
	}
	//Присваиваем начальные значения
	for (i = 0; i < n; i++) {
		L[i] = 0.0;
		M[i] = 0.0;
		u[i] = g(x);
		x += h;
	}

	if (!rank) {
		u[0] = f0(time);
		M[0] = u[0];
	}
	if (rank == size - 1) u[n - 1] = f1(time);

	double tn = MPI_Wtime(); //Начальное время

	for (i = 1; i < n - 1; i++) {
		L[i + 1] = r / (1 + 2 * r - r * L[i]);
	}

	do {
		L[1] = 0;
		M[1] = u[0];
		double tmp;
		for (i = 1; i < n - 1; i++) {
			M[i + 1] = (u[i] + r * M[i]) / (1 + 2 * r - r * L[i]);
		}

		if (rank > 0)
			MPI_Send(&u[2], 1, MPI_DOUBLE, rank - 1, msgtag, MPI_COMM_WORLD);

		if (rank != size - 1)
			MPI_Recv(&tmp, 1, MPI_DOUBLE, rank + 1, msgtag, MPI_COMM_WORLD, &status);

		if (rank != size - 1)
			u[n - 1] = (r*u[n - 2] + u[n - 1] + r * tmp) / (1.0 + 2.0*r);


		if (rank == size - 1) u[n - 1] = f1(time + tau);

		for (int i = n - 1; i > 1; i--)
			u[i - 1] = u[i] * L[i] + M[i];

		if (!rank) u[0] = f0(time);

		//Обмен данными между процессами
		if (rank & 1) {
			MPI_Ssend(&u[1], 1, MPI_DOUBLE, rank - 1, msgtag, MPI_COMM_WORLD);
			MPI_Recv(&u[0], 1, MPI_DOUBLE, rank - 1, msgtag, MPI_COMM_WORLD, &status);
			if (rank != size - 1) {
				MPI_Ssend(&u[n - 2], 1, MPI_DOUBLE, rank + 1, msgtag, MPI_COMM_WORLD);
				MPI_Recv(&u[n - 1], 1, MPI_DOUBLE, rank + 1, msgtag, MPI_COMM_WORLD, &status);
			}
		}
		else {
			if (rank != size - 1) {
				MPI_Recv(&u[n - 1], 1, MPI_DOUBLE, rank + 1, msgtag, MPI_COMM_WORLD, &status);
				MPI_Ssend(&u[n - 2], 1, MPI_DOUBLE, rank + 1, msgtag, MPI_COMM_WORLD);
			}
			if (rank) {
				MPI_Recv(&u[0], 1, MPI_DOUBLE, rank - 1, msgtag, MPI_COMM_WORLD, &status);
				MPI_Ssend(&u[1], 1, MPI_DOUBLE, rank - 1, msgtag, MPI_COMM_WORLD);
			}
		}

		time += tau; //Шаг по времени
	} while (time <= tmax);

	double tk = MPI_Wtime(); //Конечное время

	//вывод
	int in, ik;

	if (!rank) in = 0; else in = 1;
	if (rank == size - 1) ik = n; else ik = n - 1;

	for (int i = 0; i < size; i++)
	{
		if (i == rank)
		{
			FILE *f;
			if (rank == 0)
				f = fopen("Temperature.txt", "w");
			else
				f = fopen("Temperature.txt", "a");
			for (j = in; j < ik; j++, l++) {
				fprintf(f, "u[%d] = %f\n", l, u[j]);
			}
			fclose(f);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
	if (!rank) {
		FILE *f;
		f = fopen("Temperature.txt", "a");
		fprintf(f, "\nВремя вычислений: %f", tk - tn);
		fclose(f);
	}
	delete[]u;
	delete[]kol;

	MPI_Finalize();
	return 0;
}



