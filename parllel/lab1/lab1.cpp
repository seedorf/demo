#include <stdio.h>
#include <math.h>
#include <mpi.h>

#define comm MPI_COMM_WORLD

const double len = 300.0;

double psi(double x) {
	return 0.9 + 2 * x * (1 - x);
}

double fi1(double t) {
	return 3 * (0.3 - 2 * t);
}

double fi2(double t) {
	return 1.38;
}

int main(int argc, char **argv) {
	
	int size, msgtg = 12, rank;
	if (MPI_Init(&argc, &argv)) return 1;
	if (MPI_Comm_size(MPI_COMM_WORLD, &size)) {
		MPI_Finalize(); return 2;
	}
	if (MPI_Comm_rank(MPI_COMM_WORLD, &rank)) {
		MPI_Finalize(); return 3;
	}
	MPI_Status status;

	double tau = 0.01, h = 1.0, a = 1.0, tmax = 1.0, tstart = 0.0;
	double R = a * a * tau / (h * h);

	int N = (int)(len / h) + 1;

	int count = N / size;
	int ost = N % size;
	if (!rank) {
		double xx = 0.0;
		double *U1 = new double[N];
		double *L1 = new double[N];
		double *M1 = new double[N];

		for(int i = 0; i < N; i++) {
			U1[i] = psi(xx);
			xx+=h;
		}
		
		L1[0] = 0; M1[0] = 1;
		U1[0] = fi1(tstart);
		U1[N - 1] = fi2(tstart);

		for(int i = 1; i < N - 1; i++) {
			L1[i] = R / (1 + 2 * R - R * 2 * L1[i - 1]);
		}

		while(tstart < tmax){
			for(int i = 1; i < N - 1; i++){
				L1[i+1] = R/(1 + 2 * R - R * L1[i]);
				M1[i+1] = (U1[i] + R * M1[i]) / (1 + 2 * R - R * L1[i]);
			}
			U1[N-1] = fi2(tstart);
			for(int i = N - 1; i > 1; i--){
				U1[i-1] = L1[i] * U1[i] + M1[i];
			}
			U1[0] = fi1(tstart);

			for(int i = 0; i < N; i++){
				U1[i] = U1[i];
			}

			tstart += tau;
		}

		for(int i = 0; i < 10; i++) {
			fprintf(stderr, "%f\n", U1[i]);
		}
		fprintf(stderr, "\n");
		for(int i = N-10; i < N; i++) {
			fprintf(stderr, "%f\n", U1[i]);
		}
		fprintf(stderr, "\n");
		fprintf(stderr, "\n");
		fprintf(stderr, "\n");

	}
	MPI_Barrier ( comm );



	if(ost) {
		if(rank == size - 1){
			count++;
			ost--;
		}
	}
	if(ost) {
		if(rank < ost)
			count++;
	}

	double xStart = 0.0;
	if(rank && rank != size - 1) {
		count+=2;
	}else {
		count++;
	}

	if(rank < ost)
		xStart = rank * count * h;
	else
		xStart = rank * count * h + ost * h;

	double tmp;
	double x = xStart;

	double *U = new double[count];
	double *L = new double[count];
	double *M = new double[count];

	for(int i = 0; i < count; i++) {
		U[i] = psi(x);
		x+=h;
	}

	L[0] = 0; M[0] = 1;
	if(!rank) {
		U[0] = fi1(tstart);
	}
	if(rank == size - 1) {
		U[count - 1] = fi2(tstart);
	}

	for(int i = 1; i < count - 1; i++) {
		L[i] = R / (1 + 2 * R - R * 2 * L[i - 1]);
	}

	do {
		for(int i = 0; i < count - 1; i++){
			M[i + 1] = (U[i] + R * M[i]) / (1 + 2 * R - R * 2 * L[i]);
		}

		if(rank) 
			MPI_Send(&U[2], 1, MPI_DOUBLE, rank - 1, msgtg, comm);

		if(rank != size - 1) {
			MPI_Recv(&tmp, 1, MPI_DOUBLE, rank + 1, msgtg, comm, &status); 
		}
		U[count - 1] = (R * U[count - 2] - U[count - 1] + R * tmp) / (1 + 2 * R);
		

		for(int i = count - 1; i > 1; i--) {
			U[i-1] = L[i] * U[i] + M[i];
		}

		if(!rank) {
			U[0] = fi1(tstart);
		}

		if(rank == size - 1) {
			U[count - 1] = fi2(tstart);		
		}

		if(rank && 1) {
			MPI_Send(&U[1], 1, MPI_DOUBLE, rank - 1, msgtg, comm);
			MPI_Recv(&U[0], 1, MPI_DOUBLE, rank - 1, msgtg, comm, &status); 

			if(rank != size - 1) {
				MPI_Send(&U[count - 2], 1, MPI_DOUBLE, rank + 1, msgtg, comm);
				MPI_Recv(&U[count - 1], 1, MPI_DOUBLE, rank + 1, msgtg, comm, &status); 
			}
		} else {
			if(rank != size - 1) {
				MPI_Recv(&U[count - 1], 1, MPI_DOUBLE, rank + 1, msgtg, comm, &status); 
				MPI_Send(&U[count - 2], 1, MPI_DOUBLE, rank + 1, msgtg, comm);
			}
			if (rank) {
				MPI_Recv(&U[0], 1, MPI_DOUBLE, rank - 1, msgtg, comm, &status); 
				MPI_Send(&U[1], 1, MPI_DOUBLE, rank - 1, msgtg, comm);	
			}
		}
		tstart += tau;
	}while(tstart < tmax);

	if (!rank) {
		fprintf(stderr, "\n");
		for(int i = 0; i < 10; i++) {
			fprintf(stderr, "%f\n", U[i]);
		}
		fprintf(stderr, "\n");
		
	}
	// if (rank == size - 1) {
	// 	fprintf(stderr, "\n");
	// 	for(int i = count - 10; i < count ; i++) {
	fprintf(stderr, "The End");
	// 		fprintf(stderr, "%f\n", U[i]);
	// 	}
	// 	fprintf(stderr, "\n");
	// }

	MPI_Finalize();
	fprintf(stderr, "The End");
	return 0;
}
	