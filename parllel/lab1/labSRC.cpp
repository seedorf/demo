#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <iostream>

double g(double x)
{
	return 0.9 + 2 * x * (1 - x);
}

double f0(double t)
{
	return 3 * (0.3 - 2 * t);
}

double f1()
{
	return 1.38;
}

void NCalc()
{
		FILE* f = fopen("res.txt", "w");
		double tau = 0.5, h = 0.001, a = 1.0;
		int l = 300;
		double t = 0.0, tmax = 100.0;
		int N = (int)(l / h) + 1;
		double r = (a * a * tau) / (h * h);
		double* U = new double[N];
		// double* Un = new double[N];

		double* L = new double[N];
		double* M = new double[N];

		for (int i = 1; i < N - 1; i++){
			L[i] = M[i] = 0;
			U[i] = g(t);
		}

		U[0] = f0(t);
		U[N - 1] = f1();
		// Un[0] = f0(t);
		// Un[N - 1] = f1();

		for (int i = 1; i < N - 1; i++) {
			L[i + 1] = r / (1 + 2 * r - r * L[i]);
		}


		while (t < tmax)
		{
			t += tau;
			L[1] = 0;
			// M[1] = Un[0];
			for (int i = 1; i < N - 1; i++)
			{
				M[i + 1] = (U[i] + r * M[i]) / (1 + 2 * r - r * L[i]);
			}
			for (int i = N - 1; i > 1; i--)
			 {
			 	 U[i - 1] = L[i] * U[i] + M[i];
			}
		
			U[0] = f0(t);
			U[N - 1] = f1();
			
			// Un[0] = f0(t);
			// Un[N - 1] = f1();
		}

		for (int i = 0; i < N; i++)
			fprintf(f, "%f\n", U[i]);
		fclose(f);

		delete[] U;
		delete[] L;
		delete[] M;

		printf("\n\n");
}



int main() {
	//YTemp();
	NCalc();
	// getc(stdin); getc(stdin);
	return 0;
}







