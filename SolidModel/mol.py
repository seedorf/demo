import matplotlib.pyplot as plt
import time
from IPython.display import HTML
import matplotlib.animation as animation
import numpy as np 
import random, base64, io


tn = 0.0
tk = 1.0
tau = 0.001
m = 1.0
n = 3
k = 1
a = 1
tt = []
while tn < tk:
    tt.append(tn)
    tn += tau

def system1():
    x = 10.0
    U = 0.0
    xx, yy = [], []
    for t in tt:
        Un = U + (tau / m) * (a/(x ** n) - k * x)
        xn = x + tau * U
        U, x = Un, xn
        xx.append(x)
        yy.append(0.5)
        tn += tau
        print(max(xx))
        return xx, yy

def system2():
    x = 0.0
    U = 5.0
    xx, yy = [], []
    for t in tt:
        print(t)
        Un = U + tau * (1.0/((1 + x) ** n) - 1.0/((1 + x) ** n))
        xn = x + tau * U
        U, x = Un, xn
        xx.append(x)
        yy.append(0.5)
        print(max(xx))
        return xx, yy

def _update_plot(i, fig, scat): 
    scat.set_offsets(([xx[i], yy[i]])) 
    return scat 
fig = plt.figure()  
ax = fig.add_subplot(111) 
ax.grid(True, linestyle = '-', color = '0.75') 
ax.set_xlim([min(xx)-0.5, max(xx)+0.5]) 
ax.set_ylim([0, 1])
# plt.plot(xx[3000], yy[1000], 'go') #Построение графика
plt.xlabel('x') #Метка по оси x в формате TeX
plt.ylabel('0.5') #Метка по оси y в формате TeX
plt.title('Движение молекулы') #Заголовок в формате TeX
# plt.grid(True) #Сетка
# plt.show() #Показать график
scat = plt.scatter(xx, yy, c = xx)
scat.set_alpha(0.8) 

anim = animation.FuncAnimation(fig, _update_plot, fargs = (fig, scat), frames = len(tt), interval = 10)
anim.save('out.mp4')

video = io.open('out.mp4', 'r+b').read()
encoded = base64.b64encode(video)
HTML(data='''
<video controls>
    <source src="data:video/mp4;base64,{0}" type="video/mp4" />
</video>'''.format(encoded.decode('ascii')))

x = 0.0
U = 5.0
s2x, s2y = [], []
for t in tt:
    Un = U + tau * (1.0/((1 + x) ** n) - 1.0/((1 - x) ** n))
    xn = x + tau * U
    U, x = Un, xn
    s2x.append(x)
    s2y.append(0.5)
print(s2x)



def _update_plot(i, fig, scat): 
    scat.set_offsets(([s2x[i], s2y[i]])) 
    return scat 
fig = plt.figure()  
ax = fig.add_subplot(111) 
ax.grid(True, linestyle = '-', color = '0.75') 
ax.set_xlim([-1, 1]) 
ax.set_ylim([0, 1])
plt.xlabel('x') #Метка по оси x
plt.ylabel('0.5') #Метка по оси y
plt.title('Движение молекулы') #Заголовок
scat = plt.scatter(s2x, s2y, c = s2x)
scat.set_alpha(0.8) 

anim = animation.FuncAnimation(fig, _update_plot, fargs = (fig, scat), frames = len(tt), interval = 10)
anim.save('out1.mp4')

video = io.open('out1.mp4', 'r+b').read()
encoded = base64.b64encode(video)
HTML(data='''
<video controls>
    <source src="data:video/mp4;base64,{0}" type="video/mp4" />
</video>'''.format(encoded.decode('ascii')))

U = [[10], [3], [2]]
x = [[0.5], [1.0], [1.5]]
n = [2,3,2]
Un, xn = [[],[],[]], [[],[],[]]
yy = [0.5]

j=0

for t in tt:
    for i in range(1,3):
        Un0 = U[0][j-1] + (tau/m) * (1.0/((x[1][j-1] + x[0][j-1]) ** n[0]) - 1.0/((x[1][j-1] + x[0][j-1]) ** n[1]))
        xn0 = x[0][j-1] + tau * Un0
        
        Un1 = U[1][j-1] + (tau/m) * ((1.0/((x[1][j-1] + x[0][j-1]) ** n[0]) - 1.0/((x[1][j-1] + x[0][j-1]) ** n[1]))
                                  +(1.0/((x[2][j-1] + x[1][j-1]) ** n[0]) - 1.0/((x[2][j-1] + x[1][j-1]) ** n[1])))
        xn1 = x[1][j-1] + tau * Un1
        
        Un2 = U[2][j-1] + (tau/m) * (1.0/((x[2][j-1]) ** n[0]) - 1.0/((x[1][j-1]) ** n[1]))
        xn2 = x[2][j-1] + tau * Un2
        
        U[0].append(Un0)
        x[0].append(xn0)
        
        U[1].append(Un1)
        x[1].append(xn1)
        
        U[2].append(Un2)
        x[2].append(xn2)
        
        yy.append(0.5)

print(x[2][2])

def _update_plot(i, fig, scat): 
    scat.set_offsets(([[x[0][i], yy[i]], [x[1][i], yy[i]], [x[2][i], yy[i]]])) 
    return scat
fig = plt.figure()  
ax = fig.add_subplot(111) 
ax.grid(True, linestyle = '-', color = '0.75') 
ax.set_xlim([0, 11]) 
ax.set_ylim([0, 1])
plt.xlabel('x') #Метка по оси x
plt.ylabel('0.5') #Метка по оси y
plt.title('Движение молекулы') #Заголовок
scat = plt.scatter([x[0], x[1], x[2]], [yy, yy, yy], c = 'b')
scat.set_alpha(0.8)

anim = animation.FuncAnimation(fig, _update_plot, fargs = (fig, scat), frames = len(tt), interval = 10)
anim.save('out2.mp4')

video = io.open('out2.mp4', 'r+b').read()
encoded = base64.b64encode(video)
HTML(data='''
<video controls>
    <source src="data:video/mp4;base64,{0}" type="video/mp4" />
</video>'''.format(encoded.decode('ascii')))