
#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include <fstream>
#include <iostream>

#define n 500
#define eps 0.001

int main(int arc, char **argv) {
    printf("1");

	int size, rank, msgtag = 12;
	MPI_Status status;

	if (MPI_Init(&arc, &argv) != MPI_SUCCESS) return 1;
	if (MPI_Comm_size(MPI_COMM_WORLD, &size) != MPI_SUCCESS) {
		MPI_Finalize();
		return 2;
	}
	if (MPI_Comm_rank(MPI_COMM_WORLD, &rank) != MPI_SUCCESS) {
		MPI_Finalize();
		return 3;
	}
    
    	MPI_Finalize();

	return 0;
}