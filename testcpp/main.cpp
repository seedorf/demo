#include"MonteCarloLib.h"
#include <mpi.h>
#include<iostream>

using namespace std;

int main(int arc, char **argv) {

    int size, rank, msgtag = 12;
	MPI_Status status;

	if (MPI_Init(&arc, &argv) != MPI_SUCCESS) return 1;
	if (MPI_Comm_size(MPI_COMM_WORLD, &size) != MPI_SUCCESS) {
		MPI_Finalize();
		return 2;
	}
	if (MPI_Comm_rank(MPI_COMM_WORLD, &rank) != MPI_SUCCESS) {
		MPI_Finalize();
		return 3;
	}


    int a = 0;
    int b = 1000;
    int h = 2000;
    double step = 0.001;

    MonteCarloCalculator calc;

    if (!rank) {
        calc.calcIntegr(a, b, h, step);
        calc.calcIntegrMPI(a, b, h, step);
        calc.calcIntegrCUDA(a, b, h, step);
    }
   
    

    MPI_Finalize();
    return 0;
}