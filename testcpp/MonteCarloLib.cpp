#include"MonteCarloLib.h"

#include <mpi.h>
#include <iostream>
#include <ctime>
#include <stdexcept>


    MonteCarloCalculator::MonteCarloCalculator() {
        
    }
    double MonteCarloCalculator::func(double x) {
        return(x / 2);
    }


    void MonteCarloCalculator::calcIntegrMPI(int a, int b, int h, double step) {
        std::cout << " MPI " << std::endl;


    }

    void MonteCarloCalculator::calcIntegrCUDA(int a, int b, int h, double step) {
         std::cout << "CUDA" << std::endl;
    }



    void MonteCarloCalculator::calcIntegr(int a, int b, int h, double step) {
        std::cout << "Seq" << std::endl;
        unsigned int startTime = clock();
        std::srand(unsigned(std::time(0)));
        int countIn = 0;
        int countAll = 0; 

        double currentStep = a;
        while(currentStep <= b) {
            int randNum = std::rand() % (h + 1);
            if (randNum <= MonteCarloCalculator::func(currentStep)) {
                countIn++;
            }
            countAll++;
            currentStep += step;
        }
    
        double square = (double) countIn / countAll * ( (b - a) * h );
        unsigned int endTime = clock();
        std:: cout<<"square = " << square << std::endl;
        std:: cout << "time" << endTime << std::endl; 
    }

   
    