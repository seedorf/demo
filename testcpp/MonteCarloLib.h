class MonteCarloCalculator{

    public:

        MonteCarloCalculator();
        double func(double x);


        void calcIntegr(int a, int b, int h, double step);

        void calcIntegrMPI(int a, int b, int h, double step);

        void calcIntegrCUDA(int a, int b, int h, double step);


};