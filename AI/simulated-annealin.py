import random
import math
import tkinter as tk
from tkinter import Frame, Canvas, BOTH

N    = 300
Tn   = 30.0
Tk   = 0.5
Alfa = 0.98
ST   = 100

class TMember:
    plan = []
    energy = 1

Current = TMember()
Working = TMember()
Best = TMember()
T = 0.0
Delta = 0.0
P = 0.0
fNew = False
fBest = False
Time = 0
Step = 0
Accepted = 0


def Swap(M):
    x = random.randint(0, N - 1) #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    do = True
    
    while do:
        y = random.randint(0, N - 1)
        if (y != x):
            do = False
    
    v = M.plan[x]
    M.plan[x] = M.plan[y]
    M.plan[y] = v
        

def New(M):
    M.plan = list(range(N))
    for i in range(N):
        Swap(M)
    M.energy = N*N# md


def CalcEnergy(M):  
    dx = [-1, 1, -1, 1]
    dy = [-1, 1, 1, -1]

    error = 0
    for x in range(N):
        for j in range(4):
            tx = x + dx[j]
            ty = M.plan[x] + dy[j]
            
            while(tx >= 0 and tx < N and ty >= 0 and ty < N):   # while(tx > 0 and tx <= N and ty > 0 and ty <= N):
                if (M.plan[tx] == ty):
                    error += 1
                tx += dx[j]
                ty += dy[j]
    M.energy = error


def Copy(MD, MS):
    for i in range(N):
        MD.plan[i] = MS.plan[i]
        MD.energy = MS.energy


def Show(M):
    print("Result:")
    for y in range(N):
        for x in range(N):
            if (M.plan[x] == y):
                print("Q", end='')
            else:
                print(".", end='')    
        print("")


# main

#init
T = Tn 
fBest = False
Time = 0
Best.energy = 100

New(Current)

New(Working) # 
New(Best)    # 

CalcEnergy(Current)
Copy(Working, Current)

while (T > Tk):
    Accepted = 0
    for Step in range(ST):
        fNew = False
        Swap(Working)
        CalcEnergy(Working)
        if Working.energy <= Current.energy:
            fNew = True
        else:
            Delta = Working.energy - Current.energy
            p = math.exp(-Delta / T)
            if (P > random.random()): # ?
                Accepted += 1
                fNew = True
        if (fNew == True):                              
            fNew = False
            Copy(Current, Working)
            if Current.energy < Best.energy:
                Copy(Best, Current)
                fBest = True
        else:
            Copy(Working, Current)
    
    print("Temp=", T, "Energy=", Best.energy)
    T *= Alfa
    Time += 1

if (fBest == True):                                     
    Show(Best)


class Example(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.parent = parent
        self.initUI()
    
    def initUI(self):
        self.parent.title("sim-ann")
        self.pack(fill=BOTH, expand=1)

        canvas = Canvas(self)
        
        size = 15
        for i in range(N):
            for j in range(N):
                if (j+i) %2 == 0:
                    canvas.create_rectangle(j*size, i*size, j*size + size, i*size + size, outline = "#fb0")
                else:
                    canvas.create_rectangle(j*size, i*size, j*size + size, i*size + size, outline = "#fb0", fill="#fb0")

                if (Best.plan[j] == i):
                    canvas.create_oval(j*size, i*size, j*size + size, i*size + size, fill="black")

                  
        canvas.pack(fill=BOTH, expand=1)

root = tk.Tk()
ex = Example(root)
root.geometry("1200x700")
root.mainloop()