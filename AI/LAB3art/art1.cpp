#include <stdlib.h>
#include <stdio.h> 
#include <string>
#include <iostream>

#define MaxItems 11
#define MaxClients 10
#define MaxClusters 4
#define Beta 1.0
#define Rho 0.9

using namespace std;


int Members[MaxClusters], Group[MaxClients], Clusters[MaxClusters][MaxItems],Sum[MaxClusters][MaxItems];
int Data[MaxClients][MaxItems] = { { 0,0,0,0,0,1,0,0,1,0,0 },
								   { 0,1,0,0,0,0,0,1,0,0,1 },
								   { 0,0,0,1,0,0,1,0,0,1,0 },
								   { 0,0,0,0,1,0,0,1,0,0,1 },
								   { 1,0,0,1,0,0,0,0,0,1,0 },
								   { 0,0,0,0,1,0,0,0,0,0,1 },
								   { 1,0,0,1,0,0,0,0,0,0,0 },
								   { 0,0,1,0,0,0,0,0,1,0,0 },
								   { 0,0,0,0,1,0,0,1,0,0,0 },
								   { 0,0,1,0,0,1,0,0,1,0,0 } };
int N, P;

void Initialize() {
	N = 0;
	for (int i = 0; i < MaxClusters; i++) {
		for (int j = 0; j < MaxItems; j++) {
			Clusters[i][j] = 0;
			Sum[i][j] = 0;
		}
		Members[i] = 0;
	}
	for (int j = 0; j < MaxClients; j++)
		Group[j] = -1;
}

void UpdateVectors(int k) {
	bool f = true;
	if (k<1 || k>MaxClusters) return;
	for (int i = 0; i < MaxItems; i++) {
		Clusters[k][i] = 0;
		Sum[k][i] = 0;
	}
	for (int j = 0; j < MaxClients; j++) {
		if (Group[j]==k) {
			if (f) {
				for (int i = 0; i < MaxItems; i++) {
					Clusters[k][i] = Data[j][i];
					Sum[k][i] = Data[j][i];
				}
				f = false;
			}
			else {
				for (int i = 0; i < MaxItems; i++)
					Clusters[k][i] = Clusters[k][i] && Data[j][i];
				for (int i = 0; i < MaxItems; i++)
					Sum[k][i] = Sum[k][i] + Data[j][i];
			}
		}
	}
}

int CreateVector(int v[MaxItems]) {
	int i = 0;
	while (Members[i] != 0) {
		i++;
		if (i > MaxClusters) return -1;
	} 
	N++;
	for (int j = 0; j < MaxItems; j++)
		Clusters[i][j] = v[j];
	Members[i] = 1;
	return i;
}

int OnesVector(int v[MaxItems]) {
	int k = 0;
	for (int j = 0; j < MaxItems; j++) {
		if (v[j] == 1)
			k++;
	}
	return k;
}

void ExecuteART1()
{
	int R[MaxItems], PE, P, E, count, s;
	bool test, exit;
	exit = false; count = 50;
	do
	{
		exit = true;
		for (int i = 0; i < MaxClients; i++)
		{
			for (int j = 0; j < MaxClusters; j++)
			{
				if (Members[j] > 0)
				{
					for (int k = 0; k < MaxItems; k++)
						R[k] = Data[i][k] && Clusters[j][k];
					PE = OnesVector(R);
					P = OnesVector(Clusters[j]);
					E = OnesVector(Data[i]);
					test = (PE / (Beta + P)) > (E / (Beta + MaxItems));
					if (test) test = PE / E < Rho;
					if (test) test = Group[i] != j;
					if (test)
					{
						s = Group[i];
						Group[i] = j;
						if (s >= 0)
						{
							Members[s]--;
							if (Members[s] == 0)
								N--;
						}
						Members[j]++;
						UpdateVectors(s);
						UpdateVectors(j);
						exit = false;
						break;
					}
				}
			}
			if (Group[i] == -1)
			{
				Group[i] = CreateVector(Data[i]);
				exit = false;
			}
		}
		count--;
	} while ((exit==false) && (count > 0));
}

void ShowClusters()
{
	for (int i = 0; i < N; i++) {
		printf("Вектор-прототип %d -    ", i);
		for (int j = 0; j < MaxItems; j++)
			printf("%d ", Clusters[i][j]);
		printf("\n");
		for (int k = 0; k < MaxClients; k++) {
			if (Group[k] == i) {
				printf("	Покупатель %d - ", k);
				for (int j = 0; j < MaxItems; j++)
					printf("%d ", Data[k][j]);
				printf("\n");
			}
		}
	}
}

void MakeAdvise(int p) {
    int max = 0;
    int best = 0;
    for (int i = 0; i < MaxItems; i++) {
        if (Data[p][i] == 0 && Sum[Group[p]][i] > max) {
            best = i;
            max = Sum[Group[p]][i];
        }
    }
    printf("Для покупателя %d ", p);
    if (best > 0) {
        printf("рекомендация: %d\n", best);
    }
    else {
        printf("нет рекомендаций\n");
    }
    printf("Уже выбраны: ");
    for (int i = 0; i < MaxItems; i++) {
        if (Data[p][i] != 0) {
            printf("  %d  ", i);
        }
    }
    printf("\n");

}

int main() {
	Initialize();
	ExecuteART1();
	ShowClusters();
	for (int i = 0; i < MaxClients; i++) {
        MakeAdvise(i);
    }
    return 0;
}
