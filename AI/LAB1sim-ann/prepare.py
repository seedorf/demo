import random
import tkinter as tk
from tkinter import Frame, Canvas, BOTH, Label

stores       = 4     # goods are equal requirement 
warehouses   = 4
num_of_goods = 30

temp_start   = 30.0
temp_end     = 0.5
alfa         = 0.98

min_cost     = 1     # for random creation cost matrix
max_cost     = 15

ST           = 100   # num of iteration for change temperature

class TMember:      # solution class
    plan = []
    energy = 1

class UIFrame(Frame):
     def __init__(self, parent):
        Frame.__init__(self, parent)
        self.parent = parent
        self.initUI()
    
     def initUI(self):
        self.parent.title("transportation")
     
        for i in range(stores):
            for j in range(warehouses):
                Label(text=i+j).grid(row = i, column = j)       

def Show():
    root = tk.Tk()
    ui = UIFrame(root)
    root.geometry("800x600")
    root.mainloop()

                
def create_cost_matrix():
    # matrix  = []
    # for i in range(warehouses):
    #     row = []
    #     for j in range(stores):
    #         cost = random.randint(min_cost, max_cost)
    #         row.append(cost)
    #     matrix.append(row)
    matrix = [[2,8,6,4],[5,13,2,11],[10,7,4,2],[6,1,22,8]]

    return matrix

def create_demand_vector():
    # demands = []
    # remainder = num_of_goods
    # for i in range(stores -1):
    #     slots = stores - i - 1
    #     demand = random.randint(1, remainder - slots)
    #     demands.append(demand)
    #     remainder -= demand
    # demands.append(remainder) # putt all reminder in last slot
    demands = [8, 2, 6, 12]
    return demands

def create_availability_vector():
    # goods = []
    # remainder = num_of_goods
    # for i in range(warehouses -1):
    #     slots = warehouses - i - 1
    #     avl = random.randint(1, remainder - slots)
    #     goods.append(avl)
    #     remainder -= avl
    # goods.append(remainder) # putt all reminder in last slot
    goods = [4, 1, 14, 9]
    return goods

def new_solution(m):
    # TODO: improve start solution via potencial-method
    # current method: north-west corner
    # generate matrix plan
    # calculate energy as plan[][] * cost [][]
    
    plan = []                       #create empty plan matrix
    for i in range(stores):
        plan.append([])
        for j in range(warehouses):
            plan[i].append(0)
            
    i = j = 0                       # fill the plan
    demands = list(demand_vector)
    availability = list(availability_vector)

    while(i < warehouses and j < stores):   # avail - i; demands - j; 
        # find min of demands/avail
        print("i=", i, "j=", j)

        min_num = min(availability[i], demands[j])
        # minus each of them 
        availability[i] -= min_num
        demands[j] -= min_num
        #print into matrix
        plan[i][j] = min_num
        #do next step
        if (availability[i] == 0):
            i += 1
        else:
            j += 1

    m.plan = list(plan)

    energy = 0
    for i in range(stores):
        for j in range(warehouses):
            energy += m.plan[i][j] * cost_matrix[i][j]
    m.energy = energy


current = TMember()
working = TMember()
best    = TMember()

temperature = 0.0
delta = 0.0
p = 0.0             # probability 
fNew = False
fBest = False

time = 0
step = 0
accepted = 0


# main-----------------------

#init

temperature = temp_start
fBest = False
time = 0
best.energy = 100

cost_matrix = create_cost_matrix()
demand_vector = create_demand_vector()
availability_vector = create_availability_vector()

new_solution(current)

new_solution(working)
new_solution(best)

#Show()