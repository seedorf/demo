import random
import math
import tkinter as tk
from tkinter import Frame, Canvas, BOTH, Label

stores       = 30    # goods are equal requirement 
warehouses   = 30
num_of_goods = 300            #30

temp_start   = 30.0
temp_end     = 0.5
alfa         = 0.98

min_cost     = 1     # for random creation cost matrix
max_cost     = 15

ST           = 100   # num of iteration for change temperature   100

class TMember:      # solution class
    plan = []
    energy = 1

class UIFrame(Frame):
     def __init__(self, parent):
        Frame.__init__(self, parent)
        self.parent = parent
        self.initUI()
    
     def initUI(self):
        self.parent.title("transportation")
     
        for i in range(stores):
            for j in range(warehouses):
                Label(text=best.plan[i][j]).grid(row = i, column = j)       

def show(m):
    root = tk.Tk()
    ui = UIFrame(root)
    root.geometry("800x600")
    root.mainloop()

                
def create_cost_matrix():
    matrix  = []
    for i in range(warehouses):
         row = []
         for j in range(stores):
             cost = random.randint(min_cost, max_cost)
             row.append(cost)
         matrix.append(row)

    return matrix

def create_demand_vector():
     demands = []
     remainder = num_of_goods
     for i in range(stores -1):
         slots = stores - i - 1
         demand = random.randint(1, remainder - slots)
         demands.append(demand)
         remainder -= demand
     demands.append(remainder) # putt all reminder in last slot
     return demands

def create_availability_vector():
     goods = []
     remainder = num_of_goods
     for i in range(warehouses -1):
         slots = warehouses - i - 1
         avl = random.randint(1, remainder - slots)
         goods.append(avl)
         remainder -= avl
     goods.append(remainder) # putt all reminder in last slot
     return goods

def calc_energy(m):
    energy = 0
    for i in range(stores):
        for j in range(warehouses):
            energy += m.plan[i][j] * cost_matrix[i][j]
    m.energy = energy

def new_solution(m):
   
    plan = []                       #create empty plan matrix
    for i in range(stores):
        plan.append([])
        for j in range(warehouses):
            plan[i].append(0)
            
    i = j = 0                       # fill the plan
    demands = list(demand_vector)
    availability = list(availability_vector)

    while(i < warehouses and j < stores):   # avail - i; demands - j; 
        # find min of demands/avail
        min_num = min(availability[i], demands[j])
        # minus each of them 
        availability[i] -= min_num
        demands[j] -= min_num
        #print into matrix
        plan[i][j] = min_num
        #do next step
        if (availability[i] == 0):
            i += 1
        else:
            j += 1

    m.plan = list(plan)
    # calculate energy
    energy = 0
    for i in range(stores):
        for j in range(warehouses):
            energy += m.plan[i][j] * cost_matrix[i][j]
    m.energy = energy

def copy_solution(w,c):
    w.plan = list(c.plan)
    w.energy = c.energy

def swap(m):
    # 1 find random store
    while True:
        first_wh = random.randint(0, stores - 1)
        checkSum = 0
        for i in range(len(m.plan[first_wh])):
            if(m.plan[first_wh][i] > 0):
                checkSum += 1
        if (checkSum > 1):
            break
    
    # 2 find random deal
    deals = list(m.plan[first_wh])
    # Reservoir sampling
    elemCount = 0
    elem = 0
    firstElemIndex = 0

    for i in range(len(deals)):
        if (deals[i] > 0):
            randomNum = random.randint(0, elemCount)   # refact
            if randomNum == 0:
                elem = deals[i]
                firstElemIndex = i
            elemCount += 1
    
    # -- find second warehouses with deal --
    second_wh = first_wh
    secondElemndex = -1
    do = True
    while (do):
        second_wh = random.randint(0, stores - 1)
        for i in range(len(m.plan[second_wh])):
            if(second_wh != first_wh and m.plan[second_wh][i] > 0 and i != firstElemIndex):
                secondElemndex = i
                do = False
                break

    # -- calculation --
    
    m.plan[first_wh][firstElemIndex] -= 1
    m.plan[second_wh][firstElemIndex] += 1

    m.plan[first_wh][secondElemndex] += 1
    m.plan[second_wh][secondElemndex] -= 1


#init

current = TMember()
working = TMember()
best    = TMember()

temperature = 0.0
delta = 0.0
p = 0.0             # probability 
fNew = False
fBest = False

time = 0
step = 0
accepted = 0

# main

temperature = temp_start
fBest = False
time = 0
best.energy = 100

cost_matrix = create_cost_matrix()
demand_vector = create_demand_vector()
availability_vector = create_availability_vector()

new_solution(current)
new_solution(working)
new_solution(best)

copy_solution(working, current) 
swap(working)

while(temperature > temp_end ):
    accepted = 0
    for step in range(ST):
        fNew = False
        swap(working)
        calc_energy(working)
        if working.energy <= current.energy:
            fNew = True
        else:
            delta = working.energy - current.energy
            p = math.exp( -delta / temperature)
            if (p > random.random()):
                accepted += 1
                fNew = True 
        if (fNew == True):
            fNew = False
            copy_solution(current, working)
            if current.energy < best.energy:
                copy_solution(best, current)
                fBest = True
        else:
            copy_solution(working, current)
    print("Temp=", temperature, "Energy=", best.energy)
    temperature *= alfa
    time += 1

for i in range(10):
    for j in range(10):
        print(best.plan[i][j])
if (fBest == True):
    qqqq = 2
   # show(best)
