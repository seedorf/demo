import random
import math
import tkinter as tk
from tkinter import Frame, Canvas, BOTH, Label

stores       = 9    # goods are equal requirement 3
warehouses   = 9    # 3
num_of_goods = 500           #30

min_cost     = 1     # for random creation cost matrix
max_cost     = 15

alfa = 1.0
beta = 5.0
delta_tovar = 1

colony_size = stores * warehouses
steps = 50
default_ferment = 1.0 / stores #1.0 деление очень помогло
inc_ferment = 100

def create_cost_matrix():
    matrix  = []
    for i in range(warehouses):
         row = []
         for j in range(stores):
             cost = random.randint(min_cost, max_cost)
             row.append(cost)
         matrix.append(row)

    return matrix

def create_ferment_matrix():
    matrix  = []
    for i in range(warehouses):
         row = []
         for j in range(stores):
             ferment = default_ferment
             row.append(ferment)
         matrix.append(row)

    return matrix
    # odor incr  + = q / cost

def create_demand_vector():
     demands = []
     remainder = num_of_goods
     for i in range(stores -1):
         slots = stores - i - 1
         demand = random.randint(1, remainder - slots)
         demands.append(demand)
         remainder -= demand
     demands.append(remainder) # putt all reminder in last slot
     return demands

def create_availability_vector():
     goods = []
     remainder = num_of_goods
     for i in range(warehouses -1):
         slots = warehouses - i - 1
         avl = random.randint(1, remainder - slots)
         goods.append(avl)
         remainder -= avl
     goods.append(remainder) # putt all reminder in last slot
     return goods

def new_empty_solution():
    plan = []                       #create empty plan matrix
    for i in range(stores):
        plan.append([])
        for j in range(warehouses):
            plan[i].append(0)
    return plan

def get_new_solution():
    plan = []                       #create empty plan matrix
    for i in range(stores):
        plan.append([])
        for j in range(warehouses):
            plan[i].append(0)
            
    i = j = 0                       # fill the plan
    demands = list(demand_vector)
    availability = list(availability_vector)

    while(i < warehouses and j < stores):   # avail - i; demands - j; 
        # find min of demands/avail
        min_num = min(availability[i], demands[j])
        # minus each of them 
        availability[i] -= min_num
        demands[j] -= min_num
        #print into matrix
        plan[i][j] = min_num
        #do next step
        if (availability[i] == 0):
            i += 1
        else:
            j += 1
    # print(plan)
    return plan
   
def print_info():
    print("solution: ",solution)
    #s = sum(sum(solution, []))
    #print("sum: ",s)
    print("cost: ", cost_matrix)
    print("demands: ", demand_vector)
    print("avails: ", availability_vector)
    sum = 0
    for i in range(stores):
        for j in range(stores):
            sum = sum + solution[i][j] * cost_matrix[i][j]
    print("sum: ", sum)

def get_next_node(wh):
    chance_list = list()
    for i in range(stores):
        if (demands[i] > 0):
           ch = (ferment_matrix[wh][i] ** alfa) / (cost_matrix[wh][i] ** beta) 
        else:
            ch = 0
        #print("ch", i, "=", ch)
        chance_list.append(ch)
    return chance_list.index(max(chance_list))

def get_sum_cost():
    temp_sum = 0
    for i in range(stores):
        for j in range(stores):
            temp_sum += new_solution[i][j] * cost_matrix[i][j]
    return temp_sum
            

def update_odor(sc):
    for i in range(stores):
        for j in range(stores):
            ferment_matrix[i][j] += new_solution[i][j] / sc
            
cost_matrix = create_cost_matrix()
ferment_matrix = create_ferment_matrix()
demand_vector = create_demand_vector()

availability_vector = create_availability_vector()

solution = get_new_solution()

old_solution = 9999999999999999999999999999999999999999999999999999999999999999999

# start main
#---
for live_index in range(500):
    demands = list(demand_vector)
    availability = list(availability_vector)

    # make solution start
        
    warehouse_set = set(range(warehouses)) 
    new_solution = new_empty_solution()

    while(len(warehouse_set) > 0):
        rand_ware = random.sample(warehouse_set, 1) #choose random wh
        cur_warehouse = int(rand_ware[0])
        #считаем шансы для каждого магазина и определяем следующий узел
        
        next_node = get_next_node(cur_warehouse)
         #print(next_node)
        new_solution[cur_warehouse][next_node] += delta_tovar
        demands[next_node] -= delta_tovar
        availability[cur_warehouse] -= delta_tovar
        
        sum_cost = get_sum_cost()
        update_odor(sum_cost)

        if(availability[cur_warehouse] < 1):
            warehouse_set.remove(cur_warehouse)
        # выбираем в какой пихнуть
        # если после этого склад пуст, то убираем его из сета
    sum_cost = get_sum_cost()
    update_odor(sum_cost)
    if(sum_cost < old_solution):
        old_solution = sum_cost
        print("sum_cost = ", old_solution)

    # make solution end


# end
# пофиксить распределение при генерации стартового решения(потребностей)
# поиграться с параметрами alpha beta и другими