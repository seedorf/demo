import random
import math

# constants

N    = 8
Tn   = 30.0
Tk   = 0.5
Alfa = 0.98
ST   = 100

class TMember:
    plan = []
    energy = 1


Current = TMember()
Working = TMember()
Best = TMember()
T = 0.0
Delta = 0.0
P = 0.0
fNew = False
fBest = False
Time = 0
Step = 0
Accepted = 0


# funcs

def Swap(M):
    x = random.randint(0, N - 1)
    do = True
    
    while do:
        y = random.randint(0, N - 1)
        if (y != x):
            do = False
    
    v = M.plan[x]
    M.plan[x] = M.plan[y]
    M.plan[y] = v
        

def New(M):
    M.plan = list(range(N))
    for i in range(N):
        Swap(M)


def CalcEnergy(M):  # error can acc
    dx = [-1, 1, -1, 1]
    dy = [-1, 1, 1, -1]

    error = 0
    for x in range(N):
        for j in range(4):
            tx = x + dx[j]
            ty = M.plan[x] + dy[j]
            
            while(tx >= 0 and tx < N and ty >= 0 and ty < N):   # while(tx > 0 and tx <= N and ty > 0 and ty <= N):
                if (M.plan[tx] == ty):
                    error += 1
                tx += dx[j]
                ty += dy[j]
    M.energy = error


def Copy(MD, MS):
    for i in range(N):
        MD.plan[i] = MS.plan[i]
        MD.energy = MS.energy


def Show(M):
    print("Result:")
    for y in range(N):
        for x in range(N):
            if (M.plan[x] == y):
                print("Q", end='')
            else:
                print(".", end='')    
        print("")


# main

#init
T = Tn 
fBest = False
Time = 0
Best.energy = 100

New(Current)

New(Working) # added by my
New(Best)    # added by my

CalcEnergy(Current)
Copy(Working, Current)

while (T > Tk):
    Accepted = 0
    for Step in range(ST):
        fNew = False
        Swap(Working)
        CalcEnergy(Working)
        if Working.energy <= Current.energy:
            fNew = True
        else:
            Delta = Working.energy - Current.energy
            p = math.exp(-Delta / T)
            if (P > random.random()):
                Accepted += 1
                fNew = True
        if (fNew == True):                              #refact
            fNew = False
            Copy(Current, Working)
            if Current.energy < Best.energy:
                Copy(Best, Current)
                fBest = True
        else:
            Copy(Working, Current)
    
    print("Temp=", T, "Energy=", Best.energy)
    T *= Alfa
    Time += 1

if (fBest == True):                                     #refact
    Show(Best)

